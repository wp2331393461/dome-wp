package com.jk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWpApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWpApplication.class, args);
    }

}

